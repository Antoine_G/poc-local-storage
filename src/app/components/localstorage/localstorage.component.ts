import { Component, OnInit } from '@angular/core';
import { CookieStorage, LocalStorage, SessionStorage, WebStorageService } from 'ngx-store';

@Component({
  selector: 'app-localstorage',
  templateUrl: './localstorage.component.html',
  styleUrls: ['./localstorage.component.scss']
})
export class LocalstorageComponent implements OnInit {
  @LocalStorage({key: 'userNames'}) userNames = [];

  constructor() {  }

  registerPseudo(pseudo : string){
    this.userNames.push(pseudo);
  }

  delPseudo(id : number){
    this.userNames.splice(id, 1);
  }

  ngOnInit() {
  }
}
