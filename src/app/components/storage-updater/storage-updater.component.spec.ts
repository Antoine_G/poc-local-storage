import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StorageUpdaterComponent } from './storage-updater.component';

describe('StorageUpdaterComponent', () => {
  let component: StorageUpdaterComponent;
  let fixture: ComponentFixture<StorageUpdaterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StorageUpdaterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StorageUpdaterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
