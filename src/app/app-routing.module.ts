import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LocalstorageComponent } from './components/localstorage/localstorage.component';
import { StorageUpdaterComponent } from './components/storage-updater/storage-updater.component';
const routes: Routes = [
    {
        path: '',
        redirectTo: 'add',
        pathMatch: 'full'
    },
    {
        path: 'add',
        component: LocalstorageComponent,
    },
    {
        path: 'update',
        component: StorageUpdaterComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {useHash: true})],
    exports: [RouterModule]
})
export class AppRoutingModule { }
